# GLM-SBM

Implementation of AMP-BP for the GLM-SBM model. See this [article](https://arxiv.org/abs/2303.09995).


## Requirements
python3, numpy, scipy

For $N=10^4$ nodes and $\alpha=3$, less than 4GB of memory are needed. The computation takes a few minutes for ten experiments.


## Use
```
python AMP-BP.py alpha lambda rho
```

It prints a comma-separated list of the overlaps and free-entropies obtained for each experiment.


## License
This code is distributed under the [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) license.

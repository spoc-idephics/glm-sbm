import sys
import numpy as np

import _AMP_BP as AB


N = 10000
alpha = float(sys.argv[1])
M = int(N/alpha)

c = 5
wRademacher = False
initInformative = False
param = float(sys.argv[2])
paramDeltaI = False  # True to use DeltaI parameterization, False to use lambda parameterization
rho = float(sys.argv[3])

_inter = lambda t: 0  # parametric damping. 0 for no damping, 1 for freezing damping
maxIter, minIter = 500, 30
varInit = N**-0.5
err = 1e-4

Nexp = 10


def correspondenceParamsLambda(N, c, _lambda):
    d = np.sqrt(c)*_lambda
    ci, co = c+d, c-d
    return ci, co
def correspondenceParamsDeltaI(N, c, logR):
    r = 10**logR
    b = (2*c+r)/(N+r)
    co = N*(b-np.sqrt(b**2-4*c**2/(N**2+N*r)))/2
    ci = 2*c-co
    return ci, co

ci, co = correspondenceParamsDeltaI(N, c, param) if paramDeltaI else correspondenceParamsLambda(N, c, param)


overlapS, overlapW, Fs_Finfos = [], [], []

for n in range(Nexp):
    ws = AB._ws(M, wRademacher)
    F = AB._F(N, M)

    ss = 1*(np.dot(F, ws)>0)
    cs = AB._cs(ci, co)
    L, edgesIn, edgesInT, corrLN = AB.createGraph(ss, ci, co)
    pS = AB._pS(ss, rho)
    
    logChis, chisT, psis, marginals, a, v, goPrev = AB.initPrior(N, L, M, varInit, F) if not initInformative else AB.initInformative(N, L, M, varInit, F, corrLN, ss, ws)

    oSpr, oWpr = [0]*10, [0]*10

    for t in range(maxIter):        
        logChisN, chisTN, marginalsN, FBp = AB.stepBP(logChis, marginals, psis, corrLN, edgesIn, edgesInT, cs, pS)
        aN, vN, psisN, goPrevN, FAmp = AB.stepAMP(a, v, chisT[:,0], goPrev, F, wRademacher)
        
        inter = _inter(t)
        logChis, chisT, marginals = inter*logChis+(1-inter)*logChisN, inter*chisT+(1-inter)*chisTN,\
                                  inter*marginals+(1-inter)*marginalsN
        a, v, psis, goPrev = inter*a+(1-inter)*aN, inter*v+(1-inter)*vN,\
                             inter*psis+(1-inter)*psisN, inter*goPrev+(1-inter)*goPrevN
        
        oS, oW = AB.overlapS(marginals, ss, rho), AB.overlapW(a, ws, wRademacher)
        #print(oS, oW)
        #print(FBp, FAmp)
        
        oSpr.append(oS)
        oSpr.pop(0)
        oWpr.append(oW)
        oWpr.pop(0)
        if t>minIter and np.std(oSpr)<err and np.std(oWpr)<err:
            break
        
    overlapS.append(np.mean(oSpr))
    overlapW.append(np.mean(oWpr))
    Finfo = AB.phiInfo(ss, edgesInT, corrLN, alpha, ci, co, rho, wRademacher)
    Fs_Finfos.append(FBp+FAmp-Finfo)
    
s = "{}, {}, {}, " + ", ".join(['{:.4}']*3*Nexp)
print(s.format(alpha, param, rho, *overlapS, *overlapW, *Fs_Finfos))